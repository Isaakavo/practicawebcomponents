class Buscador extends HTMLElement {
  constructor() {
    super();

    this.props = {
      search: '',
    };

    this.attachShadow({ mode: 'open' });

    this.shadowRoot.innerHTML = `
      <link rel="stylesheet" href="./styles/buscador.css" />

      <div class="search-bar">
        <div>
          <input class="search" type="text" id="search" placeholder="Search" />
        </div>

        <div class="buttonContainer">
          <button id="search-btn" class="searchButton">Search</button>
          <button id="clear-btn" class="searchClear">Clear</button>
        </div>
      </div>
    `;

    this.shadowRoot
      .querySelector('#search')
      .addEventListener('input', this.onUpdateInput);
    this.shadowRoot
      .querySelector('#search-btn')
      .addEventListener('click', this.onSearch);
    this.shadowRoot
      .querySelector('#clear-btn')
      .addEventListener('click', this.onClear);

    this.shadowRoot.addEventListener('keypress', this.onEnterPress);

    this.onUpdateInput = this.onUpdateInput.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.onClear = this.onClear.bind(this);
    this.onEnterPress = this.onEnterPress.bind(this);
  }
  connectedCallback() {
    Object.keys(this.props).forEach((propName) => {
      if (this.hasOwnProperty(propName)) {
        let value = this[propName];
        delete this[propName];
        this[propName] = value;
      }
    });

    this.updateChildren();
  }

  set search(value) {
    this.props.search = value;

    this.updateChildren();
  }

  get search() {
    return this.props.search;
  }

  onUpdateInput(event) {
    const value = event.target.value;

    this.dispatchEvent(
      new CustomEvent('search-elem', {
        bubbles: true,
        composed: true,
        detail: {
          value,
        },
      })
    );
  }

  onSearch(event) {
    this.dispatchEvent(
      new CustomEvent('on-search', {
        bubbles: true,
        composed: true,
      })
    );
  }

  onEnterPress(event) {
    if (event.key === 'Enter') {
      this.dispatchEvent(
        new CustomEvent('on-search', {
          bubbles: true,
          composed: true,
        })
      );
    }
  }
  onClear() {
    this.dispatchEvent(
      new CustomEvent('clear-elem', {
        bubbles: true,
        composed: true,
      })
    );
  }

  updateChildren() {
    this.shadowRoot.querySelector('#search').value = this.props.search;
  }
}

export default Buscador;
