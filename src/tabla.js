class Tabla extends HTMLElement {
  constructor() {
    super();

    this.props = {
      users: {},
    };

    this.attachShadow({ mode: 'open' });

    this.shadowRoot.innerHTML = `
      <link rel="stylesheet" href="./styles/tablacss.css" />
      <div id="tableContainer">
        <table id="tabla">
          <thead>
            <tr>
              <th>ID</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody id="tbody"></tbody>
        </table>
        <div id="buttonContainer"> 
        <button id="prev-page"><< Prev</button>
        <button id="next-page">Next >></button>
        </div>
      </div>
    `;
    this.shadowRoot
      .querySelector('#prev-page')
      .addEventListener('click', this.onPrevPage);
    this.shadowRoot
      .querySelector('#next-page')
      .addEventListener('click', this.onNextPage);
  }

  connectedCallback() {
    Object.keys(this.props).forEach((propName) => {
      if (this.hasOwnProperty(propName)) {
        let value = this[propName];
        delete this[propName];
        this[propName] = value;
      }
    });
  }

  set users(value) {
    this.props.users = value;
    this.updateTable();
  }

  set page(value) {
    this.props.page = value;
  }

  removeRow(parent) {
    while (parent.hasChildNodes()) {
      parent.removeChild(parent.firstChild);
    }
  }

  onPrevPage() {
    this.dispatchEvent(
      new CustomEvent('prev-page', {
        bubbles: true,
        composed: true,
      })
    );
  }

  onNextPage() {
    this.dispatchEvent(
      new CustomEvent('next-page', {
        bubbles: true,
        composed: true,
      })
    );
  }

  updateTable() {
    const tableBody = this.shadowRoot.querySelector('#tbody');
    this.removeRow(tableBody);

    this.props.users.data.map((elem) => {
      let tr = document.createElement('tr');
      let id = document.createElement('td');
      let firstName = document.createElement('td');
      let lastName = document.createElement('td');
      let email = document.createElement('td');

      id.innerText = elem.id;
      firstName.innerText = elem.first_name;
      lastName.innerText = elem.last_name;
      email.innerText = elem.email;

      tr.appendChild(id);
      tr.appendChild(firstName);
      tr.appendChild(lastName);
      tr.appendChild(email);

      tableBody.appendChild(tr);
    });
  }
}

export default Tabla;
