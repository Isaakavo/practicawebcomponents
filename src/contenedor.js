class Contenedor extends HTMLElement {
  constructor() {
    super();

    this.state = {
      search: '',
      users: {},
      userSearch: {
        isSet: false,
      },
      page: 1,
    };
    this.employee = {};

    this.attachShadow({ mode: 'open' });

    this.shadowRoot.innerHTML = `
      <link rel="stylesheet" href="./styles/contenedor.css" />
      <div class="card">
      <slot></slot>
      </div>
    `;

    this.addEventListener('search-elem', this.onUpdateInput);
    this.addEventListener('on-search', this.onSearch);
    this.addEventListener('clear-elem', this.onClear);

    this.addEventListener('prev-page', this.onPrevPage);
    this.addEventListener('next-page', this.onNextPage);

    this.onUpdateInput = this.onUpdateInput.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.onClear = this.onClear.bind(this);

    this.onNextPage = this.onNextPage.bind(this);
    this.onPrevPage = this.onPrevPage.bind(this);
  }

  connectedCallback() {
    this.getUsers(this.state.page);
  }

  onSearch() {
    this.state.userSearch.data = this.state.users.data.filter((user) => {
      return (
        user.first_name === this.state.search ||
        user.last_name === this.state.search
      );
    });
    if (this.state.userSearch.data.length !== 0) {
      this.state.userSearch.isSet = true;
    } else {
      this.state.search = '';
      this.state.userSearch.isSet = false;
      alert('Dato no encontrado');
    }
    this.updateChildren();
  }

  onClear() {
    this.state.search = '';
    this.state.userSearch.isSet = false;

    this.updateChildren();
  }

  onUpdateInput(event) {
    this.state.search = this.capitalizeFirstLetter(event.detail.value);
  }

  onPrevPage(event) {
    if (this.state.page > 1) {
      this.state.page--;
    } else {
      this.showAlert();
    }

    this.getUsers(this.state.page);

    this.updateChildren();
  }

  onNextPage(event) {
    if (this.state.page !== this.state.users.total_pages) {
      this.state.page++;
    } else {
      this.showAlert();
    }

    this.getUsers(this.state.page);

    this.updateChildren();
  }

  async getUsers(page) {
    const response = await fetch(`https://reqres.in/API/users?page=${page}`);
    if (response.ok) {
      const user = await response.json();
      this.state.users = user;
    }

    this.updateChildren();
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  showAlert() {
    alert('No hay mas paginas :C');
  }

  updateChildren() {
    this.querySelector('buscador-elem').search = this.state.search;
    this.querySelector('tabla-elem').users = this.state.userSearch.isSet
      ? this.state.userSearch
      : this.state.users;
  }
}

export default Contenedor;
