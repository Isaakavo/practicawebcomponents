import Contenedor from './contenedor.js';
import Buscador from './buscador.js';
import Tabla from './tabla.js';

customElements.define('contenedor-elem', Contenedor);
customElements.define('buscador-elem', Buscador);
customElements.define('tabla-elem', Tabla);
